#!/bin/sh
#
# Use the following command to create the words-with-count (compaction) and training topic
#

docker-compose exec broker \
  kafka-topics --create --topic words-with-count \
  --partitions 12 --replication-factor 1 \
  --if-not-exists --config cleanup.policy=compact \
  --zookeeper zookeeper:2181

docker-compose exec broker \
  kafka-topics --create --topic training \
  --partitions 12 --replication-factor 1 \
  --if-not-exists \
  --zookeeper zookeeper:2181
