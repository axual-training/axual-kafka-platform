#!/bin/bash
#
# Use this script to create the "interaction-alert" topic to be used for balance alerts
#

docker-compose exec broker \
  kafka-topics --create --topic interaction-alert \
  --partitions 12 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181

